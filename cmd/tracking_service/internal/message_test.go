/*
 * Copyright (C) 2021 Orange & contributors
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 *
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA  02110-1301, USA.
 */

package internal

import (
	"bytes"
	"encoding/json"
	"strings"
	"testing"
)

func Test_message_json_encoded(t *testing.T) {
	expected := []byte(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(`{
"@type":"ci-job",
"@timestamp":"2019-10-27T09:57:31Z",
"template":{"name":"Test","version":"0.1.0"},
"ci":{
	"user":{"id":666,"login":"mmmm1998"},
	"project":{"id":333,"path":"https://gitlab.com/example/project.git"},
	"build":{"ref":"reference"},
	"job":{"id":888,"name":"test-job","stage":"check","url":"https://gitlab.com/example/project/jobs/888"},
	"pipeline":{"id":999,"url":"https://gitlab.com/example/project/pipelines/999"},
	"runner":{"id":111,"description":"My fabulous runner","tags":"fabulous","version":"1"}
	}
}`, "\n", ""), "\t", ""), "  ", ""))
	message := Message{MessageType: "ci-job",
		Timestamp:           Timestamp("2019-10-27T09:57:31Z"),
		TemplateDescription: TemplateDescription{Name: "Test", Version: "0.1.0"},
		CiInformation: CiInformation{User: CiUser{Id: 666, Login: "mmmm1998"},
			Project: CiProject{
				Id:   333,
				Path: "https://gitlab.com/example/project.git",
			},
			Build: CiBuild{CommitReference: "reference"},
			Job: CiJob{
				Id:    888,
				Name:  "test-job",
				Stage: "check",
				Url:   "https://gitlab.com/example/project/jobs/888",
			},
			Pipeline: CiPipeline{
				Id:  999,
				Url: "https://gitlab.com/example/project/pipelines/999",
			},
			Runner: CiRunner{
				Id:          111,
				Description: "My fabulous runner",
				Tags:        "fabulous",
				Version:     "1",
			}}}
	if result, err := json.Marshal(message); err != nil {
		t.Fatalf("Error while serializing message: %v", err)
	} else if !bytes.Equal(expected, result) {
		t.Fatalf("Encoding error\nExpected:\n%s\nGot:\n%s", string(expected), string(result))
	}
}
